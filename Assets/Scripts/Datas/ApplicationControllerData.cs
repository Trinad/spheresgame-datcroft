﻿using UnityEngine;

namespace Assets.Scripts.Datas
{
    // загрузка данных с mysql
    public class ApplicationControllerData
    {
        public string AssetBundleURL = "http://q92045rh.bget.ru/";

        public string assetBundleName = "game-bundle";

        public string assetName = "MyAsset";

        public float colorOffset = 0.005f;

        public int StartScore;

        public int ItemsCount = 5;

        public int Speed = 50;

        public float YItem;

        public float ItemSizeOffset = 10;

        public float DeltaTime = 30f;
    }
}