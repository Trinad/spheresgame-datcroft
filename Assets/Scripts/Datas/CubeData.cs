﻿using Assets.Scripts.Controllers;
using System.Linq;
using Assets.Scripts.GameObjects;
using UnityEngine;

namespace Assets.Scripts.Datas
{
    public class CubeData : ItemData
    {
        public int Size { get; private set; }

        public CubeData(ItemSizeType sizeType) : base(sizeType)
        {
            Size = (int)sizeType;
        }
    }
}