﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Controllers;
using Manager.Base;
using UnityEngine;

namespace Assets.Scripts.Datas
{
    public class GameItemsControllerData : BaseData
    {
        public GameObject[] GameObjects { get; set; }
        public int InstanceCount { get; set; }
        public int CurrentDifficult { get; set; }
        public float DeltaTime { get; set; }

        public float TimeInSeconds;

        public void DifficultUp()
        {
            CurrentDifficult = ApplicationController.CurrentDifficult;

            DeltaTime = 1 / (float)CurrentDifficult;
        }
    }
}
