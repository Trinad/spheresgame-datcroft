﻿using System.Collections.Generic;
using Assets.Scripts.Controllers;
using System.Linq;
using Assets.Scripts.GameObjects;
using UnityEngine;

namespace Assets.Scripts.Datas
{
    public class SphereData : ItemData
    {
        public float Radius { get; private set; }
        public SphereData(ItemSizeType radius) : base(radius)
        {
            Radius = (int)radius;
        }
    }
}