﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Datas
{
    public class GameControllerData : BaseData
    {
        public AssetBundle Asset;
        public Dictionary<Type, Object[]> Items;
    }
}