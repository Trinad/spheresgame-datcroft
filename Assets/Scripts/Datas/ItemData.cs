﻿using System;
using System.Collections;
using System.Linq;
using Assets.Scripts.Controllers;
using Assets.Scripts.GameObjects;
using Assets.Scripts.Operation;
using UnityEngine;

namespace Assets.Scripts.Datas
{
    public abstract class ItemData : BaseData, IDisposable
    {
        public float Speed { get; set; }
        public Texture2D Texture2D { get; set; }
        public GameObject ToolTip { get; set; }
        public GameObject Effect { get; set; }
        public int Score { get; set; }
        public ItemSizeType SizeType { get; set; }
        public Material ItemMaterial { get; set; }
        public float YMin { get; set; }

        protected ItemData(ItemSizeType sizeType)
        {
            SizeType = sizeType;
        }

        public void Release()
        {
            ItemMaterial = null;

            Effect = null;

            ToolTip = null;
        }

        public void Dispose()
        {
            ApplicationController.Instance.AssetErase();
        }
    }
}