﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    public Toggle Toggle;

    void OnEnable()
    {
        Toggle.isOn = SettingsData.GenerateTextureToggle;
    }
   
    public void SetSettings()
    {
        SettingsData.GenerateTextureToggle = Toggle.isOn;

//        ApplicationController.Instance.IsTextureGenerate = SettingsData.GenerateTextureToggle;
    }
}
