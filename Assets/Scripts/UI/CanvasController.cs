﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{
    public static CanvasController Instance
    {
        get
        {
            return canvasController;
        }
    }
    [SerializeField]
    private RectTransform[] uiItems;

    public List<RectTransform> UiItems;

    private static CanvasController canvasController;

    public Canvas Canvas { get; private set; }

    SettingsController settingsController;


    void Awake()
    {
        canvasController = this;

        Canvas = GetComponent<Canvas>();

        UiItems = new List<RectTransform>(uiItems);

        settingsController = UiItems.Find(o => o.name == "Panel").GetComponent<SettingsController>();

        SetItemActive("Main", true);

        SetItemActive("Game", false);
    }
    void Start()
    {

    }

    public void SetItemActive(string name, bool open)
    {
        UiItems.Find(o => o.name == name).gameObject.SetActive(open);
    }

    public void SettingsPanel(bool open)
    {
        UiItems.Find(o => o.name == "Panel").gameObject.SetActive(open);

        UiItems.Find(o => o.name == "Buttons").gameObject.SetActive(!open);
    }

    
}
