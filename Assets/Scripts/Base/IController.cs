﻿using Assets.Scripts.Datas;
using System;
using Assets.Scripts.ActionManager;

namespace Assets.Scripts.Base
{
    public interface IController
    {
        void StartController();

        void StopController();

        void EraseController();

        void UpdateController();
    }
}