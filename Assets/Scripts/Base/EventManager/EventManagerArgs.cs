﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.ActionManager;

namespace Assets.Scripts.Base.EventManager
{
    public class EventManagerArgs : EventArgs
    {
        public DispatcherEventType DispatcherEvent;
    }
}
