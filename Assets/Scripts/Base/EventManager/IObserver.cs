﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.ActionManager;
using Assets.Scripts.Base.EventManager;
using Assets.Scripts.Controllers;

namespace Assets.Scripts
{
    public interface IObserver
    {
        void Notify(object sender, EventArgs eventManagerArgs);
        //void Notify(object sender,  e);
    }

    public interface ISubject
    {
        event EventHandler OnAction;
    }
}
