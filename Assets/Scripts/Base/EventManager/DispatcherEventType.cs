﻿namespace Assets.Scripts.ActionManager
{
    public enum DispatcherEventType
    {
        GameControllerStart,
        LoaderControllerAssetLoaded,
        GameControllerLoadResourcesFromAsset,
        DifficultUp,
        ApplicationControllerUpdateState
    }
}
