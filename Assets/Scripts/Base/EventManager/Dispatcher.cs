﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class Dispatcher
    {
        Dictionary<ISubject, List<IObserver>> bindings = new Dictionary<ISubject, List<IObserver>>();

        public static Dispatcher Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Dispatcher();
                }
                return instance;
            }
        }

        private static Dispatcher instance;
        private Dispatcher()
        {
            if (bindings == null)
            {
                bindings = new Dictionary<ISubject, List<IObserver>>();
            }
        }

        public bool AddBind(ISubject subject, List<IObserver> observers)
        {
            if (observers.Any(observer => !AddBind(subject, observer)))
            {
                Debug.LogWarning("AddBind failed");

                return false;
            }
            return true;
        }

        public bool AddBind(ISubject subject, IObserver observer)
        {
            if (bindings.ContainsKey(subject))
            {
                if (!bindings[subject].Contains(observer))
                {
                    bindings[subject].Add(observer);

                    return BindingsUpdate(subject, observer);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                var observers = new List<IObserver>();

                observers.Add(observer);

                bindings.Add(subject, new List<IObserver>(observers));

                return BindingsUpdate(subject, observer);
            }
        }

        public bool RemoveBind(ISubject subject)
        {
            if (bindings.ContainsKey(subject))
            {
                bindings.Remove(subject);
                return true;
            }
            Debug.LogWarning("bind remove : subject was not added");
            return false;
        }

        public void RemoveAll()
        {
            bindings.Clear();

            instance = null;
        }

        bool BindingsUpdate(ISubject subject, IObserver observer)
        {
            try
            {
                subject.OnAction += observer.Notify;

                return true;
            }
            catch (Exception exception)
            {
                throw new ArgumentException("Dispatcher :: BindingsUpdate" + exception.Message);
            }
        }
    }
}
