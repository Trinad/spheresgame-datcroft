﻿namespace Assets.Scripts.Base
{
    public interface IGameController :IObserver, ISubject, IItems<IController>, IController
    {
    }
}