﻿using UnityEngine;

namespace Assets.Scripts.Base
{
    public interface ILoaderItems : ISubject, IItems<AssetBundle>, IController
    {
    }
}