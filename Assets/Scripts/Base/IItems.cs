﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Base
{
    public interface IItems<TItemData>
    {
        Dictionary<Type, TItemData> Items { get; }
    }
}