﻿using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Base
{
    public interface IResourceItems : ISubject, IObserver, IItems<Object[]>, IController
    {
        T[] GetData<T>() where T : Object;
        GameObject[] GetGameObjects(string tag);
        T GetData<T>(string name, string tag) where T : Object;
        T GetData<T>(string name) where T : Object;
    }
}