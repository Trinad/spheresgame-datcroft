﻿using Assets.Scripts.Datas;

namespace Assets.Scripts.Base
{
    public interface IInit
    {
        void Init(BaseData data);
    }
}