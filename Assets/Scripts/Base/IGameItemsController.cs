﻿using System.Collections;

namespace Assets.Scripts.Base
{
    public interface IGameItemsController :IObserver, ISubject, IItems<IList>
    {
        void AssetErase();
    }
}