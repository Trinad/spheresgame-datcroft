﻿using UnityEngine;

namespace Assets.Scripts.Base
{
    public interface IGameItem<out TData>
    {
        TData Data { get; }
        void ToCollect();
    }
}