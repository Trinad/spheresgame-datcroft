﻿using AssetBundles;
using Assets.Scripts.Base;
using Assets.Scripts.Datas;
using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.ActionManager;
using Assets.Scripts.Base.EventManager;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Controllers
{
    internal class LoaderController : ILoaderItems, IDisposable
    {
        #region Singletone
        public static LoaderController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LoaderController();
                }
                return instance;
            }
        }

        private static LoaderController instance;
        private LoaderController()
        {
            Debug.Log("new LoaderController");
        }
        #endregion

        private MonoBehaviour mono;
        private GameObject loaderGameObject;

        #region Dispatcher
        public void Notify(DispatcherEventType dispatcherEventType)
        {
            throw new NotImplementedException();
        }

        //public event Action<DispatcherEventType> OnAction;
        #endregion

        #region ILoaderItems
        public Dictionary<Type, AssetBundle> Items { get; private set; }

        public string assetBundleName;
        public string assetName;

        public void StartController()
        {
            assetBundleName = ApplicationController.Instance.Data.assetBundleName;
            assetName = ApplicationController.Instance.Data.assetName;

            Items = new Dictionary<Type, AssetBundle>();
            loaderGameObject = new GameObject("Loader");
            mono = loaderGameObject.AddComponent<MonoBehaviour>();

            mono.StartCoroutine(StartCorotine());
        }

        private IEnumerator StartCorotine()
        {
            yield return mono.StartCoroutine(Initialize());
            string error;

            yield return mono.StartCoroutine(InstantiateGameObjectAsync(assetBundleName, assetName));

            var asset = AssetBundleManager.GetLoadedAssetBundle(assetBundleName, out error);

            Items.Add(typeof(AssetBundle), asset.m_AssetBundle);

            if (OnAction != null)
            {
                OnAction(this,
                        new EventManagerArgs() { DispatcherEvent = DispatcherEventType.LoaderControllerAssetLoaded });

                //OnAction(DispatcherEventType.LoaderControllerAssetLoaded);
            }
        }

        protected IEnumerator Initialize()
        {
            AssetBundleManager.SetSourceAssetBundleURL(ApplicationController.Instance.Data.AssetBundleURL);
            var request = AssetBundleManager.Initialize();
            if (request != null)
                yield return mono.StartCoroutine(request);
        }

        protected IEnumerator InstantiateGameObjectAsync(string assetBundleName, string assetName)
        {

            float startTime = Time.realtimeSinceStartup;

            AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(GameObject));
            if (request == null)
                yield break;
            yield return mono.StartCoroutine(request);

            float elapsedTime = Time.realtimeSinceStartup - startTime;
            Debug.Log(assetName + " loaded successfully in " + elapsedTime + " seconds");
        }

        public void StopController()
        {
            mono.StopAllCoroutines();
        }

        public void EraseController()
        {
            StopController();

            Dispatcher.Instance.RemoveBind(this);

            Object.Destroy(loaderGameObject);

            Items.Clear();

            Debug.Log("LoaderController: Items Count - " + Items.Count);

            Items = null;

            instance = null;
        }

        public void UpdateController()
        {

        }
        #endregion
        public void Dispose()
        {
        }

        public event EventHandler OnAction;
    }
}