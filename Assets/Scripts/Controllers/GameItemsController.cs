﻿using Assets.Scripts.Base;
using Assets.Scripts.Datas;
using Assets.Scripts.GameObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.ActionManager;
using Assets.Scripts.Base.EventManager;
using Assets.Scripts.Operation;
using Manager.Base;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class GameItemsController : IGameItemsController, IController
    {
        #region Singletone
        private static GameItemsController instance;
        public static GameItemsController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameItemsController();
                }

                return instance;
            }
        }
        private GameItemsController()
        {
            controllerData = new GameItemsControllerData();

            Debug.Log("new GameItemsController");
        }

        #endregion

        LoaderHelper loaderHelper;

        GameItemsControllerData controllerData;

        public Dictionary<Type, IList> Items { get; private set; }

        private Factory.Factory _factory;

        private List<Action> instanceActions;

        bool isResourcesLoaded = false;

        #region Dispatcher

        //public event Action<DispatcherEventType> OnAction;
        public void Notify(object sender, EventArgs eventManagerArgs)
        {
            if (((EventManagerArgs)eventManagerArgs).DispatcherEvent == DispatcherEventType.GameControllerLoadResourcesFromAsset)
            {
                var operation = new LoadResourcesToPoolManagerOperation(controllerData);

                operation.ExecuteOperation((() =>
                {
                    controllerData = operation.Data;

                    _deltaTime = 1 / (float)controllerData.CurrentDifficult;

                    timeInSeconds = Time.time + _deltaTime;

                    isStoped = false;

                    isResourcesLoaded = controllerData.IsResourcesLoaded = true;

                    Debug.Log("instance _deltaTime: " + _deltaTime);

                    ApplicationController.Instance.ControllerUpdater += UpdateController;
                }));
            }

            if (((EventManagerArgs)eventManagerArgs).DispatcherEvent == DispatcherEventType.DifficultUp)
            {
                controllerData.DifficultUp();

                _deltaTime = 1 / (float)controllerData.CurrentDifficult;

                Debug.Log("instance _deltaTime: " + _deltaTime);
            }
        }
        #endregion

        #region IGameItemsController
        public void StartController()
        {
            var pool = new GameObject().AddComponent<PoolManager>();

            loaderHelper = new GameObject("loaderHelper").AddComponent<LoaderHelper>();

            _factory = new Factory.Factory(this);

            Items = new Dictionary<Type, IList>
            {
                {typeof (Cube), new List<GameObject>()},
                {typeof (Sphere), new List<GameObject>()}
            };

            instanceActions = new List<Action>()
            {
                AddRandomSphere,
                AddRandomCube
            };
        }

        bool isStoped;

        public void StopController()
        {
            isStoped = true;

            isResourcesLoaded = false;

            instanceActions.Clear();

            loaderHelper.StopAllCoroutines();

        }
        public void EraseController()
        {
            StopController();

            Dispatcher.Instance.RemoveBind(this);

            foreach (var values in Items.Values)
            {
                foreach (var obj in values)
                {
                    ((GameObject)obj).GetComponent<PlayebleObject>().OnBreak = null;
                }

                values.Clear();
            }

            Items.Clear();

            if (PoolManager.Instance)
            {
                PoolManager.Instance.Erase();
            }

            MonoBehaviour.Destroy(loaderHelper);

            instance = null;
        }

        private float _deltaTime;

        private float timeInSeconds;
        public void UpdateController()
        {
            if (!isResourcesLoaded) { return; }

            if (Time.time > timeInSeconds)
            {
                AssetErase();

                timeInSeconds = Time.time + _deltaTime;

                if (instanceActions == null || isStoped)
                {
                    return;
                }

                instanceActions[UnityEngine.Random.Range(0, instanceActions.Count)]();
            }
        }
        public void AssetErase()
        {
            loaderHelper.StartCoroutine(AsyncAssetErase());
        }

        private IEnumerator AsyncAssetErase()
        {
            var async = Resources.UnloadUnusedAssets();
            yield return async;
        }

        #endregion

        #region instanceActions


        private void AddRandomCube()
        {
            loaderHelper.StartCoroutine(ForEachCube());
        }

        IEnumerator ForEachCube()
        {
            for (int i = 0; i < controllerData.InstanceCount; i++)
            {
                var cube = _factory.GetCube();

                if (cube == null) yield break;

                cube.OnBreak += OnBreak;

                Items[typeof(Cube)].Add(cube.gameObject);
            }

            yield return null;
        }

        private void AddRandomSphere()
        {
            loaderHelper.StartCoroutine(ForEachSphere());
        }

        IEnumerator ForEachSphere()
        {
            for (int i = 0; i < controllerData.InstanceCount; i++)
            {
                var sphere = _factory.GetSphere();

                if (sphere == null)
                {
                    yield break;
                }

                sphere.OnBreak += OnBreak;

                Items[typeof(Sphere)].Add(sphere.gameObject);
            }

            yield return null;
        }

        #endregion

        #region OnBreakHandle

        private void OnBreak(IGameItem<ItemData> obj)
        {
            if (obj is Cube)
            {
                //HandleObject<Cube>((Cube)obj);

                var cube = (Cube)obj;
                if (cube.IsPlayerBreak)
                {
                    ApplicationController.Instance.AddScore(cube.Data.Score);
                }
                var items = Items[typeof(Cube)];
                if (Items[typeof(Cube)].Count > 0)
                {
                    cube.gameObject.Despawn();
                    items.Remove(cube.gameObject);
                }
            }

            if (obj is Sphere)
            {
                //HandleObject<Sphere>((Sphere)obj);

                var sphere = (Sphere)obj;
                if (sphere.IsPlayerBreak)
                {
                    ApplicationController.Instance.AddScore(sphere.Data.Score);
                }
                var items = Items[typeof(Sphere)];
                if (Items[typeof(Sphere)].Count > 0)
                {
                    sphere.gameObject.Despawn();
                    items.Remove(sphere.gameObject);
                }
            }
        }

        void HandleObject<T>(T obj) where T : PlayebleObject, IGameItem<ItemData>
        {
            if (obj.IsPlayerBreak)
            {
                ApplicationController.Instance.AddScore(obj.Data.Score);
            }

            if (Items[typeof(T)].Count > 0)
            {
                obj.gameObject.Despawn();
                Items[typeof(T)].Remove(obj.gameObject);
            }
        }
        #endregion

        public event EventHandler OnAction;
    }
}