﻿using Assets.Scripts.Base;
using Assets.Scripts.Datas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.ActionManager;
using Assets.Scripts.Base.EventManager;
using Assets.Scripts.Operation;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class GameController : IGameController
    {
        #region Singletone

        private static GameController instance;

        public static GameController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameController();
                }
                return instance;
            }
        }

        private GameController()
        {
            Debug.Log("new GameController");
        }
        #endregion

        #region Dispatcher

        //public event Action<DispatcherEventType> OnAction;
        public void Notify(object sender, EventArgs eventManagerArgs)
        {
            if (((EventManagerArgs)eventManagerArgs).DispatcherEvent == DispatcherEventType.ApplicationControllerUpdateState)
            {
                if (OnAction != null)
                {
                    OnAction(this,
                        new EventManagerArgs() { DispatcherEvent = DispatcherEventType.DifficultUp });
                }
            }
        }
        #endregion

        #region IGameController

        public Dictionary<Type, IController> Items { get; private set; }

        public void StartController()
        {
            Items = new Dictionary<Type, IController>()
            {
                {typeof(ResourceController),ResourceController.Instance },
                {typeof(LoaderController),LoaderController.Instance },
                {typeof(GameItemsController),GameItemsController.Instance},
            };

            Dispatcher.Instance.AddBind((ISubject)Items[typeof(LoaderController)], (IObserver)Items[typeof(ResourceController)]);

            Dispatcher.Instance.AddBind((ISubject)Items[typeof(ResourceController)], (IObserver)Items[typeof(GameItemsController)]);
            
            foreach (var item in Items)
            {
                item.Value.StartController();
            }

            if (OnAction != null)
            {
                OnAction(this,
                        new EventManagerArgs() { DispatcherEvent = DispatcherEventType.GameControllerStart });

                //OnAction(DispatcherEventType.GameControllerStart);
            }
        }
        public void EraseController()
        {
            StopController();

            Dispatcher.Instance.RemoveBind(this);

            if (Items != null && Items.Count > 0)
            {
                foreach (var item in Items.Values)
                {
                    item.StopController();

                    item.EraseController();
                }

                Items.Clear();
            }

            Items = null;

            instance = null;
        }
        public void StopController()
        {

        }
        public void UpdateController()
        {

        }
        #endregion IGameController

        public event EventHandler OnAction;
    }
}