﻿using Assets.Scripts.Base;
using Assets.Scripts.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.ActionManager;
using Assets.Scripts.Base.EventManager;
using Assets.Scripts.Operation;
using Manager.Base;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Controllers
{
    internal class ResourceController : IResourceItems
    {
        #region Singletone
        public static ResourceController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ResourceController();
                }
                return instance;
            }
        }

        private static ResourceController instance;
        private ResourceController()
        {
            Debug.Log("new ResourceController");
        }
        #endregion

        #region Dispatcher
        //public event Action<DispatcherEventType> OnAction;
        public void Notify(object sender, EventArgs eventManagerArgs)
        {
            if (((EventManagerArgs)eventManagerArgs).DispatcherEvent == DispatcherEventType.LoaderControllerAssetLoaded)
            {
                AddResources();
            }
        }
        private void AddResources()
        {
            var asset = LoaderController.Instance.Items[typeof(AssetBundle)];

            var operation = new LoadResourcesFromAsset(asset, Items);

            operation.ExecuteOperation(() =>
            {
                OnAction(this,
                        new EventManagerArgs() { DispatcherEvent = DispatcherEventType.GameControllerLoadResourcesFromAsset });

               // OnAction(DispatcherEventType.GameControllerLoadResourcesFromAsset);
            });

        }
        #endregion

        #region IResourceItems
        public Dictionary<Type, Object[]> Items { get; private set; }
        public T[] GetData<T>() where T : Object
        {
            Object[] resource;

            if (!Items.TryGetValue(typeof(T), out resource))
            {
                throw new ArgumentException("ResourceController: resource is not contains in Items");
            }

            return (T[])resource;
        }

        public GameObject[] GetGameObjects(string tag)
        {
            Object[] resource;

            if (!Items.TryGetValue(typeof(GameObject), out resource))
            {
                throw new ArgumentException("ResourceController: resource is not contains in Items");
            }

            var result = ((GameObject[])resource).ToList().FindAll(o => o.tag == tag).ToArray();

            return result;
        }

        public T GetData<T>(string name, string tag) where T : Object
        {
            Object[] resource;

            if (!Items.TryGetValue(typeof(T), out resource))
            {
                throw new ArgumentException("ResourceController: resource is not contains in Items");
            }

            var result = resource.ToList().Find(o => ((GameObject)o).tag == tag && o.name == name);

            return (T)result;
        }

        public T GetData<T>(string name) where T : Object
        {
            Object[] resource;

            if (!Items.TryGetValue(typeof(T), out resource))
            {
                throw new ArgumentException("ResourceController: resource is not contains in Items");
            }

            var result = resource.ToList().Find(m => m.name == name);

            return (T)result;
        }


        public void StartController()
        {
            Items = new Dictionary<Type, Object[]>();
        }

        public void StopController()
        {
            Debug.Log("ResourceController: stoped");
        }

        public void EraseController()
        {
            Dispatcher.Instance.RemoveBind(this);

            foreach (var objects in Items.Values)
            {
                var objList = objects.ToList();

                objList.Clear();
            }

            Items.Clear();

            Items = null;

            instance = null;
        }

        public void UpdateController()
        {
            throw new NotImplementedException();
        }
        #endregion

        public event EventHandler OnAction;
    }
}