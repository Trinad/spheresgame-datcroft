﻿using Assets.Scripts.Base;
using Assets.Scripts.Datas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Assets.Scripts.ActionManager;
using Assets.Scripts.Base.EventManager;
using Manager.Base;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Controllers
{
    public class ApplicationController : MonoBehaviour, IController, IObserver, ISubject
    {
        private static ApplicationController instance;
        public static ApplicationController Instance
        {
            get
            {
                return instance;
            }
        }

        public bool IsTextureGenerate = false;

        private static IGameController _gameController;

        public int Score { get; private set; }
        public static int CurrentDifficult { get; private set; }
        public ApplicationControllerData Data { get { return data; } }

        public delegate void Updater();

        public Updater ControllerUpdater;

        public void AddScore(int newScore)
        {
            Score += newScore;
        }

        private ApplicationControllerData data;

        #region Dispatcher
        //        public event Action<DispatcherEventType> OnAction;

        public event EventHandler OnAction;
        public void Notify(object sender, EventArgs eventManagerArgs)
        {
            if (((EventManagerArgs)eventManagerArgs).DispatcherEvent == DispatcherEventType.GameControllerStart)
            {
                Debug.Log("GameController was started");
            }
        }
        #endregion

        #region Unity
        void OnEnable()
        {
            instance = this;
            data = new ApplicationControllerData();
            CurrentDifficult = SceneManager.GetActiveScene().buildIndex;
        }

        void Update()
        {
            if (CurrentDifficult > 0)
            {
                UpdateController();

                if (ControllerUpdater != null)
                {
                    ControllerUpdater();
                }
            }
        }
        #endregion

        #region IController
        public void StartController()
        {
            Score = data.StartScore;

            IsTextureGenerate = SettingsData.GenerateTextureToggle;

            timer = Time.time + data.DeltaTime;

            data.YItem = CanvasController.Instance.Canvas.GetComponent<RectTransform>().rect.yMin / 4;
        }

        public void StopController()
        {

        }

        public void EraseController()
        {
            if (_gameController != null)
            {
                _gameController.EraseController();
            }

            Dispatcher.Instance.RemoveAll();

            AssetErase();
        }
        public void AssetErase()
        {
            StartCoroutine(AsyncAssetErase());
        }
        private IEnumerator AsyncAssetErase()
        {
            var async = Resources.UnloadUnusedAssets();
            yield return async;
        }

        private float deltaTime;

        float timer;
        public void UpdateController()
        {
            deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

            if (Time.time > timer)
            {
                timer = Time.time + data.DeltaTime;

                CurrentDifficult++;

                if (OnAction != null)
                {
                    OnAction(this,
                        new EventManagerArgs() {DispatcherEvent = DispatcherEventType.ApplicationControllerUpdateState});
                }

                Debug.Log("Change CurrentDifficult: " + CurrentDifficult);
            }
        }

        #endregion

        #region OnGUI
        private void OnGUI()
        {

            if (SceneManager.GetActiveScene().buildIndex == 0)
            {
                return;
            }

            Timer();

            Fps();

            ScoreGUI();
        }

        private void Timer()
        {
            GUIOutput(string.Format("time: {0}", Time.time.ToString()), 70, 100, Color.red);
        }

        private void ScoreGUI()
        {
            GUIOutput(string.Format("Score: {0}", Score.ToString()), 50, 50, Color.green);
        }

        private void Fps()
        {
            float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);

            GUIOutput(text, 60, 100, new Color(0.0f, 0.0f, 0.5f, 1.0f));
        }

        private void GUIOutput(string text, float y, int fontSize, Color color)
        {
            var w = Screen.width;
            var h = Screen.height;

            GUIStyle style = new GUIStyle();

            Rect rect = new Rect(0, h - y, w, h * 2 / 100f);
            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = h * 2 / fontSize;
            style.normal.textColor = color;

            GUI.Label(rect, text, style);
        }
        #endregion

        #region Level/Scene

        public void LoadLevel(int level)
        {
            if (level > 0)
            {
                CanvasController.Instance.SetItemActive("Buttons", false);
                CanvasController.Instance.SetItemActive("Panel", false);
                CanvasController.Instance.SetItemActive("Game", true);

                EraseController();

                CurrentDifficult += level;

                Debug.Log(string.Format("CurrentDifficult = {0}", level));

                SceneManager.LoadSceneAsync(1);
            }

            if (level == 0)
            {
                EraseController();
                CurrentDifficult = level;
                Debug.Log(string.Format("NextLevel = {0}", level));
                SceneManager.LoadSceneAsync(level);
                Destroy(gameObject);
                Destroy(CanvasController.Instance.gameObject);
                Destroy(Camera.main.gameObject);
            }

        }
        public void Exit()
        {
            EraseController();

            Application.Quit();
        }

        private void OnLevelWasLoaded(int level)
        {
            if (level == 0)
            {
                return;
            }

            Debug.Log(string.Format("OnLevelWasLoaded = {0}", level));

            StartController();

            var gameControllerData = new GameControllerData();

            _gameController = GameController.Instance;

            Dispatcher.Instance.AddBind(_gameController, this);

            Dispatcher.Instance.AddBind(this, _gameController);

            _gameController.StartController();

            if (OnAction != null)
            {
                OnAction(this,
                        new EventManagerArgs() { DispatcherEvent = DispatcherEventType.ApplicationControllerUpdateState });
            }
        }

        #endregion

    }
}