﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using Assets.Scripts.GameObjects;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Factory
{
    public class TextureGenerator : Generator<ItemData>
    {
        public static TextureGenerator Instance
        {
            get
            {
                if (inctance == null)
                {
                    inctance = new TextureGenerator();
                }

                return inctance;
            }
        }

        delegate Texture2D Create(int size, int sizeOffset);
        static TextureGenerator inctance;

        private TextureGenerator()
        {
        }

        Dictionary<ItemSizeType, Create> Creaters;

        public override Object Generate(ItemData data)
        {
            if (data == null)
            {
                Debug.LogError("TextureGenerator: data is not null");
                return null;
            }

            return generator(data);
        }

        Object generator(ItemData data)
        {
            switch (data.SizeType)
            {
                case ItemSizeType.Micro:
                    return CreateTexture(32, 4);
                case ItemSizeType.Small:
                    return CreateTexture(64, 3);
                case ItemSizeType.Medium:
                    return CreateTexture(128, 2);
                case ItemSizeType.Large:
                    return CreateTexture(256, 1);
                default:
                    {
                        Debug.LogWarning("TextureGenerator: Undefined Cube sizeType");
                        return CreateTexture(32, 4);
                    }
            }
        }

        Color color;
        private Texture2D CreateTexture(int size, int sizeOffset)
        {
            var texture = new Texture2D(size, size);

            color = new Color()
            {
                a = 1,
                r = Random.Range(0.01f, 1),
                g = Random.Range(0.01f, 1),
                b = Random.Range(0.01f, 1),
            };

            Color[] colors = new Color[size * size];
            int x = 0;

            for (int i = 0; i < colors.Length; i++)
            {
                if (x > size)
                {
                    color.r = (i / (float)x) * ApplicationController.Instance.Data.colorOffset * sizeOffset;
                    x = 0;
                }

                colors[i] = color;
                x++;
            }

            texture.SetPixels(colors);
            texture.Apply();

            return texture;
        }


    }
}