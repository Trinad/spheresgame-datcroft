﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Factory
{
    public abstract class GameObjectGenerator : Generator<ItemData>
    {
        protected virtual Vector3 GetPosition()
        {
            var canvasX = Random.Range(100, CanvasController.Instance.Canvas.pixelRect.width - 100);

            var posRandom = new Vector3(canvasX, Screen.height + 100, CanvasController.Instance.Canvas.transform.position.z + 30);

            //var posRandom = new Vector3(Random.Range(0, Screen.width), Screen.height, CanvasController.Instance.Canvas.transform.position.z + 30);

            var pos = Camera.main.ScreenToWorldPoint(posRandom);

            Vector3 position = pos;

            return position;
        }
    }
}
