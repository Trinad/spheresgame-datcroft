﻿using Assets.Scripts.Datas;
using Assets.Scripts.GameObjects;
using System;
using Assets.Scripts.Controllers;
using Manager.Base;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Factory
{
    internal class CubeGenerator : GameObjectGenerator
    {
        public override Object Generate(ItemData itemData)
        {
            var cubeData = (CubeData)itemData;

            Vector3 position = GetPosition();

            var obj = PoolManager.Spawn("MyCube", position);//GameObject.Instantiate(cubeData.ItemObject);

            if (obj == null) return null;

            var instanceSize = cubeData.Size * ApplicationController.Instance.Data.ItemSizeOffset;

            obj.transform.localScale = new Vector3(instanceSize, instanceSize, instanceSize);

            if (obj == null)
            {
                throw new ExecutionEngineException("Cube Instance Error");
            }

            var cube = obj.GetComponent<Cube>();

            if (cube == null)
            {
                //Debug.Log("Add Cube Script");

                cube = obj.AddComponent<Cube>();
            }

            cube.Init(cubeData);

            //cube.ToCollect();

            return cube;
        }
    }
}