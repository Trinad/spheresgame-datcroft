﻿using Assets.Scripts.Datas;
using Assets.Scripts.GameObjects;
using System;
using Assets.Scripts.Controllers;
using Manager.Base;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Factory
{
    public class SphereGenerator : GameObjectGenerator
    {
        public override Object Generate(ItemData itemData)
        {
            var sphereData = (SphereData)itemData;

            var position = GetPosition();

            var obj = PoolManager.Spawn("MySphere", position);//GameObject.Instantiate(sphereData.ItemObject);

            if (obj == null) return null;

            var instanceSize = sphereData.Radius * ApplicationController.Instance.Data.ItemSizeOffset;

            obj.transform.localScale = new Vector3(instanceSize, instanceSize, instanceSize);

            if (obj == null)
            {
                throw new ExecutionEngineException("Sphere Instance Error");
            }
            var sphere = obj.GetComponent<Sphere>();

            if (sphere == null)
            {
                //Debug.Log("Add Sphere Script");

                sphere = obj.AddComponent<Sphere>();
            }

            sphere.Init(sphereData);

            return sphere;
        }
    }
}