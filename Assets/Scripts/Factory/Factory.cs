﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Base;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using Assets.Scripts.GameObjects;
using Assets.Scripts.Operation;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Factory
{
    public class Factory
    {
        private SphereGenerator sphereGenerator;
        private CubeGenerator cubeGenerator;

        private Material material;

        public Factory(IGameItemsController data)
        {
            sphereGenerator = new SphereGenerator();

            cubeGenerator = new CubeGenerator();
        }

        public Sphere GetSphere()
        {
            Sphere sphere = null;

            var sphereData = new SphereData((ItemSizeType)Random.Range(1, 5));

            var operation = new LoadSphereDataOperation(sphereData);

            var textureOperation = new GetTextureOperation(sphereData);

            textureOperation.ExecuteOperation((() =>
            {
                operation.ExecuteOperation((() =>
                {
                    sphere = (Sphere) sphereGenerator.Generate(sphereData);

                    sphere.ToCollect();
                }));
            }));

            return sphere;
        }

        public Cube GetCube()
        {
            Cube cube = null;

            var cubeData = new CubeData((ItemSizeType)Random.Range(1, 5));

            var operation = new LoadCubeDataOperation(cubeData);

            var textureOperation = new GetTextureOperation(cubeData);

            textureOperation.ExecuteOperation((() =>
            {
                operation.ExecuteOperation((() =>
                {
                    cube = (Cube)cubeGenerator.Generate(cubeData);

                    cube.ToCollect();
                }));

            }));

            return cube;
        }
    }
}