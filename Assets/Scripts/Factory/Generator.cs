﻿using Assets.Scripts.Datas;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Factory
{
    public abstract class Generator<T> where T : BaseData
    {
        public abstract Object Generate(T data);
    }
}