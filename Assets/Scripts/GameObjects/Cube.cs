﻿using Assets.Scripts.Base;
using Assets.Scripts.Datas;
using Assets.Scripts.Factory;
using System;
using Assets.Scripts.Controllers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.GameObjects
{
    public class Cube : PlayebleObject, IGameItem<CubeData>
    {
        public CubeData Data { get; private set; }

        public override void Init(BaseData data)
        {
            base.Init(data);
            this.Data = (CubeData)data;
        }
        public void ToCollect()
        {
            var render = gameObject.GetComponent<Renderer>();
            render.material = Data.ItemMaterial;
            render.material.mainTexture = Data.Texture2D;

#if UNITY_EDITOR
            render.material.shader = Shader.Find("Mobile/Diffuse"); // насильно заставляем работать 
#endif
        }

        private void Update()
        {
            UpdatePosition(Data);

            Touch();
        }

        public override void UpdatePosition(ItemData data)
        {
            base.UpdatePosition(data);

            transform.eulerAngles += Vector3.up;
        }

        public override void Break()
        {
            base.Break();

            if (OnBreak != null)
            {
                OnBreak(this);
            }
            OnBreak = null;

        }

        
        protected override void DestroyEffect()
        {
            if (IsPlayerBreak)
            {
                ToolTip(Data);
            }

            var effect = GameObject.Instantiate(Data.Effect, transform.position, Quaternion.identity) as GameObject;

//#if UNITY_EDITOR
//            effect.GetComponent<Renderer>().material = Data.Effect.GetComponent<Renderer>().sharedMaterial;
//                //Shader.Find("Mobile/Diffuse");
//#endif
        }
    }
}