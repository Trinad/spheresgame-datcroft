﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.GameObjects
{
    public enum ItemSizeType
    {
        Micro = 1,
        Small = 2,
        Medium = 3,
        Large = 4
    }
}
