﻿using Assets.Scripts.Base;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using Assets.Scripts.Factory;
using UnityEngine;

namespace Assets.Scripts.GameObjects
{
    public class Sphere : PlayebleObject, IGameItem<SphereData>
    {
        public SphereData Data { get; private set; }

        public override void Init(BaseData data)
        {
            base.Init(data);

            this.Data = (SphereData)data;
        }

        private void Update()
        {
            UpdatePosition(Data);

            Touch();
        }

        public void ToCollect()
        {
            var render = gameObject.GetComponent<Renderer>();
            render.material = Data.ItemMaterial;
            render.material.mainTexture = Data.Texture2D;

#if UNITY_EDITOR
            render.material.shader = Shader.Find("Mobile/Diffuse");
#endif
        }

        public override void Break()
        {
            base.Break();
            
            if (OnBreak != null)
            {
                OnBreak(this);
            }
            OnBreak = null;
        }

        

        protected override void DestroyEffect()
        {
            if (IsPlayerBreak)
            {
                ToolTip(Data);
            }

            var effect = GameObject.Instantiate(Data.Effect, transform.position, Quaternion.identity) as GameObject;

//#if UNITY_EDITOR
            
//            effect.GetComponent<Renderer>().material = Data.Effect.GetComponent<Renderer>().sharedMaterial;
//#endif
        }
    }
}