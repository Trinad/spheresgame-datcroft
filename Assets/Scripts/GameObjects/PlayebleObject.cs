﻿using System;
using Assets.Scripts.Base;
using Assets.Scripts.Datas;
using Manager.Base;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.GameObjects
{
    public abstract class PlayebleObject : MonoBehaviour, IInit
    {
        public Action<IGameItem<ItemData>> OnBreak;

        public bool IsPlayerBreak = false;

        protected Collider thisCollider;

        public virtual void Init(BaseData data)
        {
            thisCollider = GetComponent<Collider>();
        }

        public virtual void UpdatePosition(ItemData data)
        {
            if (data == null)
            {
                Debug.LogWarning("UpdatePosition == null");

                return;
            }

            transform.position += Vector3.down * Time.deltaTime * data.Speed;

            if (transform.position.y < data.YMin)
            {
                IsPlayerBreak = false;

                Break();
            }
        }

        protected void Touch()
        {
            for (int i = 0; i < Input.touchCount; ++i)
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);

                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.collider == thisCollider)
                        {
                            IsPlayerBreak = true;

                            Break();
                        }
                    }
                }
        }

        private void OnMouseDown()
        {
            IsPlayerBreak = true;

            Break();
        }

        public virtual void Break()
        {
            DestroyEffect();
        }

        protected void ToolTip(ItemData data)
        {
            var tooltip = GameObject.Instantiate(data.ToolTip, transform.position, Quaternion.identity) as GameObject;

            if (tooltip == null)
            {
                Debug.LogWarning("ToolTip null");

                return;
            }

            tooltip.transform.LookAt(Camera.main.transform);

            tooltip.transform.Rotate(Vector3.up, 180);

            tooltip.GetComponent<TextMesh>().text = data.Score.ToString();

            var rigid = tooltip.AddComponent<Rigidbody>();

            rigid.AddForce((Vector3.up + Vector3.right) * 10, ForceMode.Impulse);

            Destroy(tooltip, 1);
        }

        protected abstract void DestroyEffect();

        //public static explicit operator PlayebleObject(UnityEngine.Object obj)
        //{
        //    return ((GameObject)obj).GetComponent<PlayebleObject>();
        //}
    }
}