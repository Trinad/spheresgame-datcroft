﻿namespace Assets.Scripts
{
    internal class Test
    {
        public class Shape
        {
        }

        public class Circle : Shape
        {
        }

        public interface IContainer<out T>
        {
            T GetItem();
        }

        public class Container<T> : IContainer<T>
        {
            private T item;

            public Container(T item)
            {
                this.item = item;
            }

            public T GetItem()
            {
                return item;
            }
        }

        private static void Main(string[] args)
        {
            IContainer<Shape> list = GetList();
        }

        public static IContainer<Shape> GetList()
        {
            return new Container<Circle>(new Circle());
        }
    }
}