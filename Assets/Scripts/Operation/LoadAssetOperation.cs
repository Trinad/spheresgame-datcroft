﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Datas;

namespace Assets.Scripts.Operation
{
    class LoadAssetOperation : Operation<LoaderControllerData>
    {
        public LoadAssetOperation(LoaderControllerData data) : base(data)
        {
        }
    }
}
