﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Operation
{
    class LoadSphereDataOperation : LoadItemDataOperation<SphereData>
    {
        public LoadSphereDataOperation(SphereData data) : base(data)
        {
        }

        protected override GameObject GetGameObject()
        {
            return ResourceController.Instance.GetData<GameObject>("SphereToolTip", "Score");
        }

        protected override Material GetMaterial()
        {
            return ResourceController.Instance.GetData<Material>("SphereMaterial");

        }

        protected override GameObject GetEffect()
        {
            var array = ResourceController.Instance.GetGameObjects("Effect");

            return array[Random.Range(0, array.Length)];
        }
    }
}
