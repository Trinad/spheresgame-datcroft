﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Base;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Operation
{
    public class LoadResourcesFromAsset : Operation<GameControllerData>
    {

        public LoadResourcesFromAsset(AssetBundle asset, Dictionary<Type, Object[]> item) : base(new GameControllerData() { Asset = asset, Items = item })
        {

        }

        public override void ExecuteOperation(Action compliteAction)
        {
            if (Data.Asset == null)
            {
                throw new Exception("AssetBundle is must be not a null");
            }

            Data.Items.Add(typeof(GameObject), (GameObject[])Data.Asset.LoadAllAssets<GameObject>());
            Data.Items.Add(typeof(Material), (UnityEngine.Material[])Data.Asset.LoadAllAssets<Material>());
            Data.Items.Add(typeof(Texture2D), (UnityEngine.Texture2D[])Data.Asset.LoadAllAssets<Texture2D>());

            Debug.Log("Get Asset Bundle Comlite");
            base.ExecuteOperation(compliteAction);
        }
    }
}
