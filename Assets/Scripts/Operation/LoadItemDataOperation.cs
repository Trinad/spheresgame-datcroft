﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using UnityEngine;

namespace Assets.Scripts.Operation
{
    abstract class LoadItemDataOperation<T> : Operation<T> where T : ItemData
    {
        public LoadItemDataOperation(T data) : base(data)
        {
            var difficult = ApplicationController.CurrentDifficult;

            Data.YMin = ApplicationController.Instance.Data.YItem;

            var size = (int)Data.SizeType;

            Data.Score = (int)(10 / size) * difficult;

            Data.Speed = (ApplicationController.Instance.Data.Speed / (float)size) + (difficult * 5f);
        }

        public override void ExecuteOperation(Action compliteAction)
        {
            Data.ToolTip = GetGameObject();

            Data.ItemMaterial = GetMaterial();

            Data.Effect = GetEffect();

            //GetTexture();

            base.ExecuteOperation(compliteAction);
        }

        protected abstract GameObject GetGameObject();

        protected abstract Material GetMaterial();

        protected abstract GameObject GetEffect();

        protected virtual void GetTexture()
        {
            var textureOperation = new GetTextureOperation(Data);

            textureOperation.ExecuteOperation((() =>
            {

            }));
        }
    }
}
