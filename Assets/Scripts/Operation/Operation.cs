﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Operation
{
    public abstract class Operation<TData>
    {
        public TData Data;

        public Operation(TData data)
        {
            Data = data;
        }
        
        public virtual void ExecuteOperation(Action compliteAction)
        {
            if (compliteAction != null)
            {
                compliteAction();
            }
        }

    }
}
