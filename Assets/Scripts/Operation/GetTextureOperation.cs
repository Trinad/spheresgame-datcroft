﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using Assets.Scripts.Factory;
using UnityEngine;
using Random = System.Random;

namespace Assets.Scripts.Operation
{
    class GetTextureOperation : Operation<ItemData>
    {
        private Texture2D[] loadedTexture2Ds;

        private TextureGenerator generator;

        public GetTextureOperation(ItemData data) : base(data)
        {
            loadedTexture2Ds = ResourceController.Instance.GetData<Texture2D>();

            generator = TextureGenerator.Instance;
        }

        public override void ExecuteOperation(Action compliteAction)
        {
            Data.Texture2D = ApplicationController.Instance.IsTextureGenerate ? GetGenerateTexture2D(Data) : GetTexture2DFromResourceController();

            base.ExecuteOperation(compliteAction);
        }

        private Texture2D GetTexture2DFromResourceController()
        {
            return loadedTexture2Ds[UnityEngine.Random.Range(0, loadedTexture2Ds.Length)];
        }

        private Texture2D GetGenerateTexture2D(ItemData data)
        {
            return (Texture2D)generator.Generate(data);
        }
    }
}
