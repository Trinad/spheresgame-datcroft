﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Controllers;
using Assets.Scripts.Datas;
using Manager.Base;
using UnityEngine;

namespace Assets.Scripts.Operation
{
    public class LoadResourcesToPoolManagerOperation : Operation<GameItemsControllerData>
    {

        public LoadResourcesToPoolManagerOperation(GameItemsControllerData data) : base(data)
        {
            Data.InstanceCount = ApplicationController.Instance.Data.ItemsCount;

            Data.CurrentDifficult = ApplicationController.CurrentDifficult;
        }

        public override void ExecuteOperation(Action compliteAction)
        {
            GetResourceData();

            PoolInit();

            base.ExecuteOperation(compliteAction);
        }

        private void GetResourceData()
        {
            Data.GameObjects = ResourceController.Instance.GetData<GameObject>();
        }

        private void PoolInit()
        {
            foreach (GameObject gameObject in Data.GameObjects)
            {
                if (gameObject.tag != "Effect" && gameObject.tag != "Score")
                {
                    PoolManager.AddPool(gameObject, 50, false);
                }
            }

            PoolManager.Instance.Init();
        }
    }
}
